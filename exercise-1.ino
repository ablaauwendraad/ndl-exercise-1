// Robert Blaauwendraad, a.blaauwendraad@student.science.ru.nl, s1084960
// Mike de Jong, mjc.dejong@student.science.ru.nl, s1063038
// Git repository: https://gitlab.science.ru.nl/ablaauwendraad/ndl-exercise-1

// Hand Gesture Sensor libraries
#include <Wire.h>
#include "paj7620.h"

/*
Notice: When you want to recognize the Forward/Backward gestures, your gestures'
reaction time must less than GES_ENTRY_TIME(0.8s).
You also can adjust the reaction time according to the actual circumstance.
*/
#define GES_REACTION_TIME 500
#define GES_ENTRY_TIME 800
#define GES_QUIT_TIME 1000
#define I2C_ADDRESS 0x43
#define I2C_ADDRESS2 0x44

// OLED Display libraries
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define KEYA D3
#define KEYB D4
#define OLED_RESET -1 // GPIO1

Adafruit_SSD1306 display(OLED_RESET);

// WiFi libraries
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

#define LISTEN_PORT 80

const char *ssid = "NDL_24G"; // name of local WiFi network in the NDL
const char *password = "RT-AC66U"; // the password of the WiFi network

MDNSResponder mdns;
ESP8266WebServer server(LISTEN_PORT);
String webPage = "<!DOCTYPE html><h1>ESP Trainer</h1>";

// Motor shield library
#include <LOLIN_I2C_MOTOR.h>

#define PWM_FREQUENCY 1000
LOLIN_I2C_MOTOR motor(DEFAULT_I2C_MOTOR_ADDRESS);
//I2C address 0x30 SEE NOTE BELOW

unsigned long roundOver = 0;

enum Choice {
    none, head, tail
};
Choice choice = none;
Choice result = none;

bool gameStarted = false;
int consecutiveWins = 0;
int requiredWins = 3;

void setup() {
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C); // initialize with the I2C address
  pinMode(KEYA, INPUT);
  pinMode(KEYB, INPUT);

  Serial.begin(115200);
  uint8_t error = paj7620Init(); // initialize Paj7620 registers
  if (error) {
    Serial.print("INIT ERROR,CODE: ");
    Serial.println(error);
  }

  WiFi.begin(ssid, password); // make the WiFi connection
  Serial.print("Start connecting.");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
  }
  Serial.print("\n");
  Serial.print("Connected to ");
  Serial.print(ssid);
  Serial.print(". IP address: ");
  Serial.println(WiFi.localIP());
  if (mdns.begin("esp8266", WiFi.localIP())) {
    Serial.println("MDNS responder started");
  }

  // make handlers for input from WiFi connection
  server.on("/", []() {
      String indexPage = webPage + " <a href=\"game\"><button>Start the game!</button></a>";
      server.send(200, "text/html", indexPage);
  });

  server.on("/game", []() {
      // Get query parameter
      String parameter = server.arg(0);
      if (parameter != NULL) {
        requiredWins = atoi(parameter.c_str());
      }

      String gamePage =
              webPage +
              "<script> function redirect() { var value = document.getElementById(\"site\").value; window.location.href = value; return ;} </script>" +
              "<form id=\"form\" >" +
              "<label for=\"site\">Choose consecutive wins:</label>"
              "<br>"
              "<select name=\"site\" id=\"site\">" +
              "<option value=\"game?wins=1\">1</option>" +
              "<option value=\"game?wins=2\">2</option>" +
              "<option selected value=\"game?wins=3\">3</option>" +
              "<option value=\"game?wins=4\">4</option>" +
              "<option value=\"game?wins=5\">5</option>" +
              "<option value=\"game?wins=6\">6</option>" +
              "<select>" +
              "<button class=\"btn\" type=\"button\" onclick=\"redirect()\">Select</button>"
              "</form>"
              "<p>Required consecutive wins:<br>" +
              requiredWins +
              "<p>Consecutive games won:<br>" +
              consecutiveWins +
              "</p>";
      server.send(200, "text/html", gamePage);
      gameStarted = true;
  });
  server.begin(); // start the server for WiFi input
  Serial.println("HTTP server started");

  //wait until motor shield ready.
  while (motor.PRODUCT_ID != PRODUCT_ID_I2C_MOTOR) {
    motor.getInfo();
  }

  display.clearDisplay();
}

void loop() {
  server.handleClient();

  display.setCursor(0, 0);
  display.setTextColor(WHITE);
  display.setTextSize(1);

  if (!gameStarted) {
    display.println("Start game\non web");
  } else if (millis() > roundOver + 2000) { // Wait to start new round
    display.clearDisplay();
    uint8_t data = 0, data1 = 0, error;
    error = paj7620ReadReg(I2C_ADDRESS, 1, &data); // Read gesture result.
    if (!error) {
      switch (data) {
        case GES_UP_FLAG:
          choice = head;
          break;
        case GES_DOWN_FLAG:
          choice = tail;
          break;
      }
    }

    if (choice == none) {
      display.println("Swipe up\nfor heads\n\nSwipe down\nfor tails");
    } else { // When gesture is made, flip coin
      result = static_cast<Choice>((rand() > RAND_MAX / 2) ? 1 : 2);
      // Output coin flip result and if guess was correct
      String coinResult;
      String gameResult;
      result == head ? coinResult = "Heads" : coinResult = "Tails";
      if (result == choice) {
        gameResult = "U won!";
        consecutiveWins++;
      } else {
        gameResult = "U lost!";
        consecutiveWins = 0;
      }
      display.setTextSize(2);
      display.println(coinResult);
      display.setTextSize(1.5);
      display.println(gameResult);
      choice = none;
      display.display();

      // Every selected amount of consecutive wins, dispense a candy
      if (consecutiveWins > 0 && consecutiveWins % requiredWins == 0) {
        motor.changeFreq(MOTOR_CH_BOTH, PWM_FREQUENCY);
        motor.changeStatus(MOTOR_CH_A, MOTOR_STATUS_CCW);

        unsigned long lastTime = millis();
        while (millis() < lastTime + 2000) {
          for (int duty = 40; duty <= 45; duty += 1) {
            motor.changeDuty(MOTOR_CH_A, duty);
          }
        }
        motor.changeStatus(MOTOR_CH_A, MOTOR_STATUS_STANDBY);
      }
      roundOver = millis();
    }
  }

  display.display(); // required to refresh the screen contents
}
